#include "CrimeAverageGraphicResponse.h"

#include "boost/algorithm/string.hpp"
#include <boost/foreach.hpp>
#include <stdlib.h>

CrimeAverageGraphicResponse::CrimeAverageGraphicResponse() : Response("CrimeAverageGraphic")
{
}

std::string CrimeAverageGraphicResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	std::vector<int> averages;
	int columnsNumber = 11;
	int lines = packet.size() / columnsNumber;

	for (int i = 0; i < columnsNumber; i++)
	{
		int crimeSum = 0;
		for (int j = 0; j < lines; j++)
		{
			BOOST_FOREACH(const boost::property_tree::ptree::value_type & value, packet.get_child(std::to_string(i)))
			{
				crimeSum += atoi(value.second.data().c_str());
			}
		}
		this->content.push_back(boost::property_tree::ptree::value_type("Result", std::to_string(averages[i])));
		return this->getContentAsString();
	}
}

