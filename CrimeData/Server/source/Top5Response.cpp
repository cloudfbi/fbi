#include "Top5Response.h"
#include "boost/tokenizer.hpp"

#include <algorithm>

Top5Response::Top5Response() :Response("Top5Cities")
{
}

std::string Top5Response::interpretPacket(const boost::property_tree::ptree & packet)
{

   std::vector<std::pair<std::string, int>> topFiveContainer;
   const int firstFive = 5;

   for (const auto& element : packet)
   {
      topFiveContainer.push_back(std::make_pair(element.first, DataTokenizer(element.second.data())));
   }

   std::sort(topFiveContainer.begin(), topFiveContainer.end(), SortHelper);

   this->content.push_back(boost::property_tree::ptree::value_type("File", "topFiveCities.txt"));

   for (int index = 0; index < firstFive; index++)
   {
      this->content.push_back(boost::property_tree::ptree::value_type(topFiveContainer[index].first,std::to_string(topFiveContainer[index].second)));
   }

   return this->getContentAsString();
  
}

int Top5Response::DataTokenizer(const std::string & cityData)
{
   typedef boost::tokenizer<boost::char_separator<char>> tokenizer;
   boost::char_separator<char> separator{ " " };

   std::string aux; 
   aux = cityData;

   std::vector<int> dataHolder;

   tokenizer tok{ cityData };

   for (const auto& data : tok)
   {
      dataHolder.push_back(atoi(data.c_str()));
   }

   int dataSum = 0;

   for (int index = 0; index < dataHolder.size(); index++)
   {
      dataSum += dataHolder[index];
   }

   return dataSum;
}

bool Top5Response::SortHelper(const std::pair<std::string, int>& firstPair, const std::pair<std::string, int>& secondPair)
{
   return (firstPair.second > secondPair.second);
}
