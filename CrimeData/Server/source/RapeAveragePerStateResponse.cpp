#include "RapeAveragePerStateResponse.h"

#include "boost/algorithm/string.hpp"
#include <stdlib.h>

RapeAveragePerStateResponse::RapeAveragePerStateResponse() : Response("RapeAveragePerState")
{
}

std::string RapeAveragePerStateResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	int count = 0;
	int RapeSum = 0;

	for (const auto& elemenet : packet)
	{
		count++;
		RapeSum += atoi(elemenet.second.data().c_str());
	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "rapeAveragePerState.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", std::to_string(RapeSum/count)));

	return this->getContentAsString();
}
