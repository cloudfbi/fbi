#include "ResponsesManager.h"

#include "TotalViolentCrimeResponse.h"
#include "RapeAveragePerStateResponse.h"
#include "Top5Response.h"
#include "TheMostFrequentCrimePerStateResponse.h"
#include "CrimeAverageGraphicResponse.h"

ResponsesManager::ResponsesManager()
{
	this->Responses.emplace("TotalViolentCrime", std::make_shared<TotalViolentCrimeResponse>());
	this->Responses.emplace("RapeAveragePerState", std::make_shared<RapeAveragePerStateResponse>());
	this->Responses.emplace("Top5Cities", std::make_shared<Top5Response>());
	this->Responses.emplace("TheMostFrequentCrimePerState", std::make_shared<TheMostFrequentCrimePerStateResponse>());
	this->Responses.emplace("CrimeAverageGraphic", std::make_shared<CrimeAverageGraphicResponse>());
}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}
