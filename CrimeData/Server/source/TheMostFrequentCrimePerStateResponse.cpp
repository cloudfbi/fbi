#include "TheMostFrequentCrimePerStateResponse.h"

#include "boost/algorithm/string.hpp"
#include <stdlib.h>

TheMostFrequentCrimePerStateResponse::TheMostFrequentCrimePerStateResponse() : Response("TheMostFrequentCrimePerState")
{
}

std::string TheMostFrequentCrimePerStateResponse::interpretPacket(const boost::property_tree::ptree& packet)
{

	int frequentCrime = 0;
	std::string key;

	for (const auto& element : packet)
	{
		if (frequentCrime < atoi(element.second.data().c_str()))
		{
			frequentCrime = atoi(element.second.data().c_str());
			key = std::get<0>(element);
		}
	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "TheMostFrequentCrimePerState.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", key));

	return this->getContentAsString();
}