#include "TotalViolentCrimeResponse.h"

#include "boost/algorithm/string.hpp"
#include <stdlib.h>

TotalViolentCrimeResponse::TotalViolentCrimeResponse() : Response("TotalViolentCrime")
{
}

std::string TotalViolentCrimeResponse::interpretPacket(const boost::property_tree::ptree & packet)
{

	int violentCrimeSum = 0;

	for (const auto& elemenet : packet)
	{
		violentCrimeSum +=  atoi(elemenet.second.data().c_str());
	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "totalViolentCrime.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", std::to_string(violentCrimeSum)));

	return this->getContentAsString();
}
