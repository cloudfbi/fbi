#pragma once

#include "Response.h"
#include <vector>
#include <string>

class TheMostFrequentCrimePerStateResponse : public Framework::Response
{
public:
	TheMostFrequentCrimePerStateResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};