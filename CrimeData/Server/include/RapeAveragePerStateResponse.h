#pragma once

#include "Response.h"
#include <vector>
#include <string>

class RapeAveragePerStateResponse : public Framework::Response
{
public:
	RapeAveragePerStateResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};