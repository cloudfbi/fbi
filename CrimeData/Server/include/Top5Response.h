#pragma once
#include "Response.h"

#include <vector>

class Top5Response : public Framework::Response
{
public:
   Top5Response();

   std::string interpretPacket(const boost::property_tree::ptree& packet);

private:
   int DataTokenizer(const std::string& cityData);
   static bool SortHelper(const std::pair<std::string, int>& firstPair, const std::pair<std::string, int>& secondPair);
};
