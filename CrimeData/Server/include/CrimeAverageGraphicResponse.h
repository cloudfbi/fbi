#pragma once

#include "Response.h"
#include <vector>
#include <string>

class CrimeAverageGraphicResponse : public Framework::Response
{
public:
	CrimeAverageGraphicResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};
