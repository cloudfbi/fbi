#pragma once

#include "Response.h"
#include <vector>
#include <string>

class TotalViolentCrimeResponse : public Framework::Response
{
public :
	TotalViolentCrimeResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};