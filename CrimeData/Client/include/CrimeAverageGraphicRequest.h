#pragma once

#include "Request.h"
#include <vector>
#include <string>

class CrimeAverageGraphicRequest : public Framework::Request
{
public:
	CrimeAverageGraphicRequest();

private:
	std::vector<std::string> m_columnValues;
};