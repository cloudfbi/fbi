#pragma once

#include "Request.h"
#include <vector>
#include <string>

using requestContainer = std::vector<std::pair<std::string, std::string>>;

class Top5Request :public Framework::Request
{
public:
	Top5Request();
private:
	requestContainer m_cities;
};