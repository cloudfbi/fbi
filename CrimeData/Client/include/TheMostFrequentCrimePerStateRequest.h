#pragma once

#include "Request.h"
#include<vector>
#include<string>
#include<list>
#include<utility>
#include <cstdlib> 

class TheMostFrequentCrimePerStateRequest : public Framework::Request
{
public:
	TheMostFrequentCrimePerStateRequest();

private:
	std::vector<std::string> m_columnValues;
};