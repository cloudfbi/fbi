#pragma once

#include <fstream>
#include <vector>
#include <string>

class Helper
{
public:

	std::vector<std::string> ColumnParser(const std::string& stateName, const int& columnToParse);
	std::vector < std::pair<std::string, std::string>> LineParserForTop5(const std::string& stateName);

private:
	std::ifstream m_file;
};