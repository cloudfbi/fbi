#pragma once

#include "Request.h"
#include <vector>
#include <string>

class TotalViolentCrimeRequest : public Framework::Request
{
public:
	TotalViolentCrimeRequest();

private:
	std::vector<std::string> m_columnValues;
};