#pragma once

#include "Request.h"
#include <vector>
#include <string>

class RapeAveragePerStateRequest : public Framework::Request
{
public:
	RapeAveragePerStateRequest();

private:
	std::vector<std::string> m_columnValues;
};