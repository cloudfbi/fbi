#include <iostream>
#include <memory>

#include "Session.h"

int main(int argc, char** argv)
{
	const auto host = "127.0.0.1";
	const auto port = "2751";

	// The io_context is required for all I/O
	boost::asio::io_context ioContext;

	RequestsManager requestsManager;

	auto violentCrime = requestsManager.getMap().at("TotalViolentCrime")->getContentAsString();
	auto rapeAverage = requestsManager.getMap().at("RapeAveragePerState")->getContentAsString();
	auto topFive = requestsManager.getMap().at("Top5Cities")->getContentAsString();
	auto frequentCrime = requestsManager.getMap().at("TheMostFrequentCrimePerState")->getContentAsString();
	auto crimeAverage = requestsManager.getMap().at("CrimeAverageGraphic")->getContentAsString();

	std::make_shared<Session>(ioContext)->run(host, port, violentCrime);
	std::make_shared<Session>(ioContext)->run(host, port, rapeAverage);
	std::make_shared<Session>(ioContext)->run(host, port, topFive);
	std::make_shared<Session>(ioContext)->run(host, port, frequentCrime);
	std::make_shared<Session>(ioContext)->run(host, port, crimeAverage);

	// Run the I/O service. The call will return when
	// the socket is closed.
	ioContext.run();

	return EXIT_SUCCESS;
}