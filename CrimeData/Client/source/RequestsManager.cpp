#include "RequestsManager.h"

#include "TotalViolentCrimeRequest.h"
#include "RapeAveragePerStateRequest.h"
#include "Top5Request.h"
#include "TheMostFrequentCrimePerStateRequest.h"
#include "CrimeAverageGraphicRequest.h"

RequestsManager::RequestsManager()
{
	this->requests.emplace("TotalViolentCrime", std::make_shared<TotalViolentCrimeRequest>());
	this->requests.emplace("RapeAveragePerState", std::make_shared<RapeAveragePerStateRequest>());
   this->requests.emplace("Top5Cities", std::make_shared<Top5Request>());
   this->requests.emplace("TheMostFrequentCrimePerState", std::make_shared<TheMostFrequentCrimePerStateRequest>());
   this->requests.emplace("CrimeAverageGraphic", std::make_shared<CrimeAverageGraphicRequest>());
}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
	return this->requests;
}
