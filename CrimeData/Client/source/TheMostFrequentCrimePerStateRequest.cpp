#include "TheMostFrequentCrimePerStateRequest.h"
#include "Helper.h"


TheMostFrequentCrimePerStateRequest::TheMostFrequentCrimePerStateRequest() : Request("TheMostFrequentCrimePerState")
{
	Helper parseHelper;

	const int totalCrimes = 11;

	int crimeSum = 0;

	for (int crimeIndex = 0; crimeIndex < totalCrimes; crimeIndex++)
	{
		this->m_columnValues = parseHelper.ColumnParser("ALASKA", crimeIndex);

		for (int index = 0; index < m_columnValues.size(); index++)
		{
			crimeSum += atoi(m_columnValues[index].c_str());
		}

		this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(crimeIndex), std::to_string(crimeSum)));
	}
}