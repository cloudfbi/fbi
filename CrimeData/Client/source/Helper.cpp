#include "Helper.h"
#include "boost/tokenizer.hpp"


std::vector<std::string> Helper::ColumnParser(const std::string & stateName, const int& columnToParse)
{
	m_file.open("C:\\Users\\tunde\\Desktop\\noCommaState(1).csv");

	std::string helperString = "ALABAMA"; 
	std::string read;

	std::vector<std::string> columnValues;

	typedef boost::tokenizer<boost::escaped_list_separator<char>> tokenizer;
	

	while (helperString != stateName)
	{
		std::getline(m_file, read);

		tokenizer tok{ read };

		helperString = *tok.begin();
	}

	std::string helperS;

	while (helperS.size() == 0)
	{
		std::getline(m_file, read);
		tokenizer tok{ read };

		int column = 0;

		for (const auto& t : tok)
		{
			if (column == columnToParse)
				columnValues.push_back(t);

			helperS = *tok.begin();

			++column;
		}
	}


	m_file.close();

	return columnValues;
}

std::vector<std::pair<std::string, std::string>> Helper::LineParserForTop5(const std::string & stateName)
{
	m_file.open("C:\\Users\\tunde\\Desktop\\noCommaState(1).csv");

	std::string readLine;

   std::vector<std::pair<std::string, std::string>> lineValues;

   std::string helperString = "ALABAMA";

   typedef boost::tokenizer<boost::escaped_list_separator<char>> tokenizer;

   while (helperString != stateName)
   {
      std::getline(m_file, readLine);

      tokenizer tok{ readLine };

      helperString = *tok.begin();
   }

   helperString.erase();

   while (helperString.size() == 0)
   {
      std::string cityName;
      std::string cityData;

      std::getline(m_file, readLine);
      
      tokenizer tok{ readLine };
      helperString = *tok.begin();

      int column = 0;
      
      for (const auto& t : tok)
      {
         if (column == 1)
         {
            cityName.append(t);
         }
         else
         {
            cityData.append(t + " ");
         }

         column++;
      }

      lineValues.push_back(std::make_pair(cityName, cityData));
   }


   return lineValues;

}
