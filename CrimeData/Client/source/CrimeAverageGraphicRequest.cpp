#include "CrimeAverageGraphicRequest.h"
#include "Helper.h"

#include <vector>
#include <iostream>
#include <string>

CrimeAverageGraphicRequest::CrimeAverageGraphicRequest() : Request("CrimeAverageGraphic")
{

	Helper parseHelper;

	std::vector<std::vector<std::string>> crimesData;
	crimesData.resize(11);

	const int columnsNumber = 11;

	for (int index = 0; index < columnsNumber; index++)
	{
		int currentColumn = index+2;
		this->m_columnValues = parseHelper.ColumnParser("ALASKA", currentColumn);
		crimesData[index].resize(m_columnValues.size());
		for(int j=0;j<m_columnValues.size();j++)
		{
			crimesData[index][j] = m_columnValues[j];
			this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(index), crimesData[index][j]));
		}
	}
}