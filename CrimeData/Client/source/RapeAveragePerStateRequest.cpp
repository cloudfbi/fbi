#include "RapeAveragePerStateRequest.h"
#include "Helper.h"

#include <vector>
#include <iostream>

RapeAveragePerStateRequest::RapeAveragePerStateRequest() : Request("RapeAveragePerState")
{

	Helper parseHelper;

	const int RapeColumn = 5;

	this->m_columnValues = parseHelper.ColumnParser("ALASKA", RapeColumn);

	for (int index = 0; index < m_columnValues.size(); index++)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(index), m_columnValues[index]));
	}
}