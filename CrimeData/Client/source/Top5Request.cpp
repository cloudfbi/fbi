#include "Top5Request.h"
#include "Helper.h"

#include <iostream>

Top5Request::Top5Request() : Request("Top5Cities")
{
   Helper helper;

   m_cities = helper.LineParserForTop5("ALASKA");

   for (int index = 0; index < m_cities.size() - 1; index++) //-1 pentru ca ultimul element nu ne trebuie
   {
      this->content.push_back(boost::property_tree::ptree::value_type(m_cities[index].first, m_cities[index].second));
   }
}
