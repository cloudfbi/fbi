#include "TotalViolentCrimeRequest.h"
#include "Helper.h"

#include <vector>
#include <iostream>

TotalViolentCrimeRequest::TotalViolentCrimeRequest() : Request("TotalViolentCrime")
{

	Helper parseHelper;

	const int violentCrimeColumn = 3;

	this->m_columnValues = parseHelper.ColumnParser("ALASKA", violentCrimeColumn);

	for (int index = 0; index < m_columnValues.size(); index++)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(index),m_columnValues[index]));
	}
}
